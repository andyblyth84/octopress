---
layout: post
title: "Push a Link to the Backend of Typo3"
date: 2013-02-21 17:06
comments: true
categories: [Blog, Web Development, Typo3, Typo3 Backend]
---
{%codeblock Push a link to the backend of Typo3 lang:php %}
    <?php
class User_AbsolutCourseUrl {

    public function getUrl($params){
        $uid = $params['row']['uid'];
        if(!is_numeric($uid) || $uid < 1) {
            return 'Non-link text';
        }
        //The next line is a sample URL showing what the Simulate Static extension does to your URLS
        $url = 'http://' . t3lib_div::getIndpEnv('HTTP_HOST')  . '/Home.6.0.html?course=' . $uid;
        //The next line returns a link which will be displayed in the backend
        return '<a href="' . $url . '" target="_blank" style="color:blue; text-decoration:underline;">' . $url . "</a>";
    }

}
?><!--obviously never close the php tag, this is to allow octopress to close the codeblock -->
{%endcodeblock %}