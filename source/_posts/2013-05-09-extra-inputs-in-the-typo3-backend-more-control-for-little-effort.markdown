---
layout: post
title: "Extra inputs in the typo3 backend (more control for little effort)"
date: 2013-05-09 15:02
comments: true
categories: [Blog, Web Development, typo3, backend, fluxform]
---

This code will add a textarea to the typo3 backend with the feildname of "caption" and render a div on the frontend with the class "caption".
The part of the second snippet which is in curly braces({}) is the reference to the data collected by the field.

In the template area:

{%codeblock Backend template (include the field) lang:html %}
<flux:flexform.field.text name="caption" label="Caption" cols="20" rows="10" />
{%endcodeblock %}

In the rendering area:

{%codeblock Frontend componant (Render the input) lang:html %}
<div class="caption">{record.header_caption}</div>
{%endcodeblock %}
