---
layout: post
title: "How to Avoid Creating a Form for a Link (Bootstrap/Typo3)"
date: 2013-02-21 16:38
comments: true
categories: [Blog, Web Development, Typo3, Fluid, Bootstrap, Nice Buttons, php, php5]
---
##Linking to a new page
In this example xx is the Typo3 page's UID
{%codeblock From one page to another lang:html %}
    <f:link.page pageUid="xx" class="css-selectors bootstrap-selectors btn" >Button Text Here</f:link.page>
{%endcodeblock %}

##Linking to another view of the same page (e.g.  multi-step login forms)
{%codeblock Carry on a multi-part form or submit a form lang:html %}
    <div class="control-group">
        <button class="css-selectors bootstrap-selectors btn" type="submit">Button Text Here</button>
    </div>
{%endcodeblock %}

