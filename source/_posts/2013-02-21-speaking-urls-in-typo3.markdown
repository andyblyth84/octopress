---
layout: post
title: "Speaking URLs in Typo3"
date: 2013-02-21 15:46
comments: true
categories: [Blog,  Intro, Web Development, Typo3, Typoscript, Speaking URLs] 
---
To begin here is the typoscript which enables "Speaking URLs" in Typo3:

{%codeblock Speaking URL enabler typoscript lang:html %}
//Speaking URL Enabeler
    config.simulateStaticDocuments = 1
    config.simulateStaticDocuments_addTitle = 100
    config.simulateStaticDocuments_pEnc = md5
{%endcodeblock %}

Include the above code in your htdocs > typoscript > site.txt and site.txt.skel files.