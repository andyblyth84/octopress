---
layout: post
title: "Using a Datepicker for Bootstrap in Typo3/Fluid"
date: 2013-02-21 16:46
comments: true
categories: [Blog, Web Development, Typo3, Fluid, Twitter Bootstrap, Datepicker, Javascript]
---
##The markup

This is the method to implement my favorite configuration of Twitter’s bootstrap datepicker.
{%codeblock Markup for the Datepicker lang:html %}
    <div class="input-append date datepicker input-custom" data-date="1980-01-01" data-date-format="yyyy-mm-dd">
        <input class="input-custom-date" size="16" type="text" value="Click on the icon to enter DOB" readonly>
    <span class="add-on"><i class="icon-calendar"></i></span>
{%endcodeblock %}

The data-date and the data-date format work like a value argument for the pop-out date picker. The readonly part is optional but stops direct input, I like this because it removes the need for complex date validation, it allows us to set the data-date-format argument to the format in the database (check phpmyadmin during development).

The CSS and JS for this plugin (and all the further options you can use) are found at [http://www.eyecon.ro/bootstrap-datepicker/](http://www.eyecon.ro/bootstrap-datepicker/)

REMEMBER TO USE $(‘.datepicker’).datepicker() TO CALL THE FUNCTION IN YOUR SITE.JS FILE