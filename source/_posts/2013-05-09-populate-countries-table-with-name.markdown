---
layout: post
title: "Populate countries table with Name"
date: 2013-05-09 14:52
comments: true
categories: [Blog, Web Development, typo3, MySQL, CSV parsing]
---

The following code copies the desired fields from a csv included table to the typo3 table for holding countries:

{%codeblock SQL for inputing the data lang:html %}

INSERT INTO abs_academy.tx_cabsacad_domain_model_country (name, iso2, iso3)
    SELECT `Common Name`, `ISO 3166-1 2 Letter Code`, `ISO 3166-1 3 Letter Code`
    FROM `TABLE 1`

{%endcodeblock %}

Including a csv of countries into your typo3 db using phpmyadmin:

    1. login to phpmyadmin
    2. find csv online of table of countries
    3. download this csv and take note of the filename and location
    4. create a target table (table to take the details)
    5. add the countries to this new table with the import button
    6. use the file uploader on the page to give phpmyadmin the correct file.
    7. check the box next to 
        “The first line of the file contains the table column names (if this is unchecked, the first line will become part of the data)”
    8. you have the required table, run the code in the next post.

Command to backup DB with phing:

phing db:data:dump && phing db:structure:dump && phing db:data:commit && phing db:structure:commit

Typo3 sql debugger 
debug_mysql_db
