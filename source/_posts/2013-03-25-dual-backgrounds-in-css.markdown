---
layout: post
title: "Dual Backgrounds in CSS"
date: 2013-03-25 14:39
comments: true
categories: [Blog, Web Development, CSS, CSS2, Dual background, Bootstrap]
---
The way to do this is to lay one over the other.

In this case we will lay the right hand background over the left as this requires (marginally) fewer lines of CSS to work.

Firstly here is the CSS for the backgrounds.

{%codeblock CSS for the background divs lang:css %}
    .left-bg{
    background: url(../img/image1.jpg);
    width: 100%;
    position: fixed;
    height: 100%;
}
.right-bg{
    background: url(../img/image2.jpg);
    width: 50%;
    position: fixed;
    right: 0px;
    height: 100%;
}
{%endcodeblock %}

And the markup to display the two backgrounds.
{%codeblock The Markup for the two backgrounds lang:html %}
    <div class="container avo-bg">
        <div class="aex-bg"></div>
    </div>
{%endcodeblock %}

NB: This is how to do it in a bootstraped project as the container is providing the full fixed height for both internal selectors.