---
layout: post
title: "Bootstrap Modals"
date: 2013-05-09 15:21
comments: true
categories: [Blog, Web Development, markup, bootstrap, css, js]
---

Here is the markup (with no content) to implement a bootstrap modal:

{%codeblock Bootstrap modal markup lang:html %}
<div class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;"><div class="modal-body"><div id="jqueryselector" class="">
    <button class="close" data-dismiss="modal">×</button>
    <h1>Title</h1>
    <p>Content</p>
</div>
{%endcodeblock %}