---
layout: post
title: "Clean view helpers in Typo3"
date: 2013-02-21 15:55
comments: true
categories: [Blog, Web Development, Typo3, Fluid, View Helpers, php, php5]
---
##M, V and no C
Need to cut out crazy controller logic and keep the code/markup nice and clean when passing parameters of a method to a view in typo3? Use a view helper!
###The View
{%codeblock View for View Helper system lang:html %}
{namespace abs=Tx_CAbsacad_ViewHelpers}
    <abs:jobAttributes ambassador="{xx}" jobattribute="1" />
    <fieldset class="pull-left">
            <dt class="list-label">
                Labeltext
            </dt>
            <ul>
                <f:for each="{calledFromViewHelper}" as="value" key="key">
                    <f:if condition="{true} == {key and value set}">
                        <f:then>
                            <li class="class">
                        </f:then>
                        <f:else>
                            <li class="otherclass">
                        </f:else>
                    </f:if>
                        {calledFromViewHelper}
                    </li>
                </f:for>
            </ul>
            <dt class="list-label">
                labeltext
            </dt>
            <dd class="list-param">
                labeltext
            </dd>
            <dt class="list-label">
                    labeltext
            </dt>
            <dd class="list-param">
               labeltext
            </dd>
        </fieldset>
{%endcodeblock %}

###The View Helper
{%codeblock View Helper lang:php %}
<?
    /*
     *
     * @package TYPO3
     * @subpackage Fluid
     * @version
     */
    class Tx_EXTENSIONNAME_ViewHelpers_ElementNameViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper {
        /**
         * Renders the word tick or cross for each number 0-9 which is related to an option on the jobattributes list.
         * @param Tx_CAbsacad_Domain_Model_Ambassador $ambassador
         * @param int $jobattribute
         * @return string of 'tick' or 'cross'
         * @author Andy Blyth <ab@contagious.uk.com>
         */
        public function render($extenstionvar, $var) {
            return $x->getVar($var);
        }
}

?>
{%endcodeblock %}

###The Function in the Model

{%codeblock Function in the Model lang:php %}
<?
     /**
            *Returns true false
            * @param int $job
            * @return boolean $hasjob
            */
            public function getHasJobattribute($job){
                /* get all jobs
                * explode current amabassador job using: explode(',', $this->getJobattributes())
                * return true if job is present, return false if not
                */
                //$jobAttribs = $this->getJobattributesAsArray();
                return in_array($job, explode(',', $this->getJobattributes()));
            }
?>
{%endcodeblock %}