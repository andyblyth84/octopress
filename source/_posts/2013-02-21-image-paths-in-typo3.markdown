---
layout: post
title: "Image paths in Typo3"
date: 2013-02-21 15:52
comments: true
categories: [Blog, Web Development, Typo3, Fluid, Image Paths, TCA, php, php5, Uploader, Markdown]
---
##TCA Syntax
The following code snippet shows an if statement which shows a user’s uploaded image if it is present or a placeholder image to keep the styling of the page.
{%codeblock A conditional calling to a TCA referencing image path lang:php %}
<?php //PHP open and close tags are included for clarity and syntax highlighting only
public function uploadAction(){
                $model = $GLOBALS['TSFE']->fe_user->getKey('ses','Tx_Extension_Domain_Model_Ambassador');
                $tca = t3lib_div::loadTCA('ttx_extension_domain_model_name');
                $uploadDir = $GLOBALS['TCA']['tx_extension_domain_model_name']['columns']['image']['config']['uploadfolder'] . '/';

                $model->setImage(Tx_CAbsacad_Utilities_FrontendUploadFile::handleUpload($_FILES['tx_extension_name']['tmp_name']['userimage'], $_FILES['tx_extension_name']['name']['userimage'],  $uploadDir));

                $feuser = $GLOBALS['TSFE']->fe_user->setKey('ses', 'Tx_Extension_Domain_Model_Namer', $model);
                $this->redirect('page');
            }
            public function pageAction(){
                $model = $GLOBALS['TSFE']->fe_user->getKey('ses','Tx_Extension_Domain_Model_Name');
                $tca = t3lib_div::loadTCA('tx_extension_domain_model_ambassador');
                $this->view->assign('x', $this->getSelectOptions($GLOBALS['TCA']['tx_extension_domain_model_ambassador']['columns']['x']['config']['items']));
                $this->view->assign('ambassador', $model);
                $uploadDir = $GLOBALS['TCA']['tx_extension_domain_model_ambassador']['columns']['image']['config']['uploadfolder'] . '/';

                if(strlen($model->getImage() > 0)){
                    $userImage =$uploadDir . $model->getImage();
                } else {
                    $userImage = t3lib_extMgm::siteRelPath('extension')  . 'Resources/Public/img/placeholderImage.png';
                }

                $this->view->assign('userimage', $userImage);
                $feuser = $GLOBALS['TSFE']->fe_user->setKey('ses', 'Tx_Extension_Domain_Model_X', $model);
                if(count($_POST) > 0){
                    $model->saveFromForm();
                    $this->redirect('nextpage');
                }
            }
?>
{%endcodeblock %}

Then in your view call the new method with the following line of HTML:

{%codeblock HTML call to the TCA calling method lang:html %}
    <f:image src="{userimage}" width="159" height="200" alt="" class="userimage" />
{%endcodeblock %}

The markup is written in Typo3 Fluid see [The Typo3/Fluid Documentation](http://wiki.typo3.org/Fluid) for more.

In making this post I had to find the Github pages markdown method for including a link. I found it [here.](http://stackoverflow.com/questions/6474045/linking-to-other-wiki-pages-on-github)
