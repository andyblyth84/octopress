---
layout: post
title: "jQuery Validator for Use in Typo3 Fluid"
date: 2013-02-21 16:59
comments: true
categories: [Blog, Web Development, Typo3, Fluid, Twitter Bootstrap,  Javascript, jQuery]
---
##Call to the function site wide
Include the following in your site.js file.

{%codeblock Call to the Validate function lang:javascript %}
    $("form").validate();
{%endcodeblock %}

##Simple markup example

The markup for a firstname and email field to ensure they are required.

{%codeblock Markup lang:html %}
    <!-- A required field -->
        <f:form.textfield name="fname" class="input-custom" required="true" />
    <!-- A forced email field which is also reqired -->
        <f:form.textfield name="email" class="input-custom" required="true" additionalAttributes="{email:'true'}" />
{%endcodeblock %}

Note: Form elements with no required=”true” or other attributes are thought of as valid so make sure valid fields are locked down (like a datepicker with no validation would have to be written as readonly with the area refusing cursor actions - oh yeah css3!).

Like so -
{%codeblock Datepicker markup lang:html %}
<!-- A datepicker field which is also reqired -->
    <div class="input-append date datepicker input-custom" data-date="1980-01-01" data-date-format="yyyy-mm-dd">
        <span class="add-on"><i class="icon-calendar"></i></span>
        <input class="input-custom-date" size="16" type="text" value="Click on the icon to set DoB" readonly date="true">
    </div>
{%endcodeblock %}